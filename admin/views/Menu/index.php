<section class="content">
    <div class="box">
        <div class="box-header">
            Listado de Menús
        </div>
        <div class="box-body">
            <table id="menuList">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>URL</th>
                        <th>Parent</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->menus as $key => $menu) : ?>
                    <tr id="<?php echo $menu[" id"]; ?>">
                        <td>
                            <?php echo $menu["id"]; ?>
                        </td>
                        <td class="editable" data-type="text" data-name="name">
                            <?php echo $menu["name"]; ?>
                        </td>
                        <td class="editable" data-type="text" data-name="url">
                            <?php echo $menu["url"]; ?>
                        </td>
                        <td>
                            <?php echo (isset($menu["parent"])) ? $menu["parent"] : 'NA' ; ?>
                        </td>
                        <td>
                            <a onclick="deleteElement(event)" data-name="<?php echo $menu["name"]; ?>" data-id="<?php echo $menu["id"]; ?>"><i class="fa fa-trash-o"></i></a></td>

                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <script>
                $(function () {

                    $('#menuList').DataTable({
                        "order": [[0, "desc"]],
                        "lengthMenu": [5, 10, 20, 50, 100]
                    });

                });

                function deleteElement(e) {
                    let el = $(e.target.parentNode);
                    var entonces = confirm(`Desea eliminar el menú ${el.data("name")}?`);
                    if (entonces) {

                        $.ajax({
                            url: "Menu/delete",
                            method: "POST",
                            data: { id: el.data("id") }
                        }).done(function (r) {
                            console.log(r);
                            response = JSON.parse(r);
                            if (response.error == 0) {
                                var parent = document.querySelector(`#${el.data("id")}`).parentNode;
                                parent.removeChild(document.querySelector(`#${el.data("id")}`));
                            } else {
                                alert(response.msg);
                            }
                        });

                    }
                }
            </script>
            <script>

                function generateInput(el, type) {
                    var leInput = document.createElement("input");
                    leInput.type = type;
                    leInput.value = el.html();
                    leInput.name = el.data("name");
                    leInput.placeholder = leInput.value;
                    leInput.onkeyup = function (e) {
                        if (e.key == "Enter") {
                            if (this.value != "") {
                                asyncUpdate(this);
                                this.parentElement.innerHTML = this.value;
                            }
                        }
                    };
                    return leInput;
                }

                $(() => {
                    $(".editable").dblclick(function () {
                        console.log($(this));
                        let el = $(this);
                        switch (el.data("type")) {
                            case "text":
                                var leInput = generateInput(el, "text");
                                el.html("");
                                el.append(leInput);
                                $(el[0].querySelector("input")).select();
                                break;
                            case "number":
                                var leInput = generateInput(el, "number");
                                el.html("");
                                el.append(leInput);
                                break;
                        }
                    });
                });

                function asyncUpdate(el) {
                    console.log($(el.parentNode.parentNode));
                    var id = $(el.parentNode.parentNode)[0].id;
                    var data = { id: id };
                    data[`${el.name}`] = `${el.value}`;
                    $.ajax({
                        url: "Menu/update",
                        method: "POST",
                        data: data
                    }).done(function (r) {
                        let respuesta = JSON.parse(r);
                        /*if (respuesta.error == 0) {
                            el.parentNode.innerHTML = el.value;
                        }*/
                        console.log(respuesta);
                    });
                }
            </script>

        </div>
    </div>
</section>