<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Crear Item</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="menuForm" method="POST" role="form" action="<?php echo URL; ?>Item/create" target="magicArea" enctype="multipart/form-data">

              <div class="box-body">
                <div class="form-group">
                  <label for="menuName">Nombre</label>
                  <input type="text" name="name" class="form-control" id="menuName" placeholder="Nombre menú">
                </div>
                <div class="form-group">
                  <label for="menuUrl">Precio:</label>
                  <input type="number" name="value" class="form-control" id="menuUrl" placeholder="0.0000">
                </div>

                <div class="form-group">
                  <label for="menuUrl">Foto:</label>
                  <input type="file" name="pic" class="form-control">
                </div>

                <textarea name="description" id="" cols="30" rows="10"></textarea>

                <select name="categorias[]" width="100%" multiple>
                  <?php foreach($this->categorias as $key => $categoria): ?>
                    <option value="<?php echo $categoria["id"];?>"><?php echo $categoria["name"];?></option>
                  <?php endforeach; ?>
                </select>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>

          <iframe id="magicArea" name="magicArea" width="100%" frameborder="0"></iframe>