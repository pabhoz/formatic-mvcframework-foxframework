<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Menu_controller extends \Fox\FoxController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {  
       /**
        * Ejemplo de relación de pertenencia
        */
       /*$u = new User(null, "test", "1234", "test@test.com", 0);
       $r = Rol::getBy("name", "Client");
       
       //$u->belongsTo("Rol", $r);      
       //$response = $u->create();
       //print_r($response);
       echo "<br>";
       
       $r->hasOne("User", $u);
       echo "<pre>";
       print_r($u);
       echo "</pre>";*/
        
       /*$categorias = ["Hombres","Mujeres","Niños"];
       foreach ($categorias as $key => $categoria) {
           $c = new Category(null, $categoria, null);
           $c->create();
       }*/
        
       /*$items = ["Camiseta","Zapato","Short"];
       foreach ($items as $key => $item) {
           $c = new Item(null, $item, 0, "", "El item $key");
           $c->create();
       }*/
        
       /*$i = Item::getById(1);
       $c1 = Category::getById(1);
       $c2 = Category::getById(3);
       
       $i->belongsToMany("Categorias",$c1,["estado"=>1]);
       print "<pre>";
       print_r($i);
       print "</pre>";
       
       $i->belongsToMany("Categorias",$c2,["estado"=>0]);
       print "<pre>";
       print_r($i);
       print "</pre>";
       
       $i->update();*/
        
        //POPULATE
        
       /* $r = Rol::getBy("name", "Client");
        print "<pre>";
        print_r($r);
        print "</pre>";
        $r->populate("one","User");
        print "<pre>";
        print_r($r);
        print "</pre>";*/
       
      $this->view->menus = Menus_bl::getMenus();
      $this->view->render($this,"index");
    }
    
    public function crear()
    {
        $this->view->menus = Menus_bl::getMenus();
        $this->view->render($this,"crear");
    }
    
    public function create(){
        $data = filter_input_array(INPUT_POST);
        $data["id"] = null;
        $data["parent"] = ($data["parent"] > 0) ? $data["parent"] : null;
        $menu = Menu::instanciate($data);
        $r = $menu->create();
        print_r(json_encode($r));
    }

     public function update(){
         $data = filter_input_array(INPUT_POST);
         $menu = Menu::getById($data["id"]);
         unset($data["id"]);
         foreach ($data as $key => $value) {
             $menu->{"set".ucfirst($key)}($value);
         }
         $r = $menu->update();
         print_r(json_encode($r));
     }
     
     public function delete(){
        // print_r($_POST);
        $id = filter_input(INPUT_POST, "id");
        $r= Menu::getById($id)->delete();
        print_r(json_encode($r));
     }
}
