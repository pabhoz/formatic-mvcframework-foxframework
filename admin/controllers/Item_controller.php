<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Item_controller extends \Fox\FoxController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {  
      //$this->view->menus = Menus_bl::getMenus();
      $this->view->render($this,"index");
    }
    
    public function crear()
    {
        $this->view->categorias = Category::getAll();
        $this->view->render($this,"crear");
    }
    
    public function create(){
        $data = filter_input_array(INPUT_POST);
        
        $categorias = $data["categorias"];
        unset($data["categorias"]);
        
        $data["id"] = null;
        $data["pic"] = "";
        $item = Item::instanciate($data);
        
        $r = $item->create();
        
        if($r["error"] == 0){
            $iResponse = Fox\Utils\Image::upload(G_PUBLIC."Items/", $_FILES["pic"], 
                $item->getName()."_".$item->getId(), 5242880, true);
            $item->setPic($iResponse);
            $item->setName("Si senorito");
            
            //relaciones multiples con categorias
            foreach ($categorias as $categoria) {
                $c = Category::getById($categoria);
                $item->belongsToMany("Categorias",$c,["estado"=>1]);
            }
            
            $r = $item->update();
        }
        if($r["error"] == 0){
            $msg = "Item creado correctamente";
        }else{
            $msg = "Error";
        }
        echo "<script>"
        . "parent.alert('$msg');</script>";
    }

     public function update(){
         $data = filter_input_array(INPUT_POST);
         $menu = Menu::getById($data["id"]);
         unset($data["id"]);
         foreach ($data as $key => $value) {
             $menu->{"set".ucfirst($key)}($value);
         }
         $r = $menu->update();
         print_r(json_encode($r));
     }
     
     public function delete(){
        // print_r($_POST);
        $id = filter_input(INPUT_POST, "id");
        $r= Menu::getById($id)->delete();
        print_r(json_encode($r));
     }
}
