<?php

class Menu_controller extends \Fox\FoxServiceController {

    function __construct() {
        parent::__construct();
    }

    public function getMenu($ordered = false) {
        $menus = Menus_bl::getMenus(); 
        if($ordered){
           $menus = Menus_bl::orderMenus($menus);
        }
        
        print_r(json_encode($menus));
    }

}
