<?php

class Menus_controller extends \Fox\FoxServiceController {

    function __construct() {
        parent::__construct();
    }


    public function getMenus($ordered = false) {
        $menus = Menus_bl::getMenus(); 
        if($ordered){
           $menus = Menus_bl::orderMenus($menus);
        }
        
        print_r(json_encode($menus));
    }
    
    public function getMenu(){
        
        $get = filter_input_array(INPUT_GET);
        
        $id = null;
        if(isset($get["id"])) {
            $id = $get["id"];
        }else{
            Fox\Core\Request::setHeader(400);
            $msg = ["msg"=>"El valor del id no ha sido provisto","error"=>1];
            Fox\Core\Penelope::printJSON($msg);
            die();
        }
        
        $populate = (isset($get["populate"])) ? true: false;
        
        $menu = Menus_bl::getMenu($id);
        
        if($populate){
           Menus_bl::populate($menu,true);
        }
        $menu = $menu->toArray();
        Fox\Core\Penelope::printJSON($menu);
    }

}
