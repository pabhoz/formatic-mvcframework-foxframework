<header>
        
    <nav>
        <?php foreach ($this->menus as $menu) : ?>
            <div class="menu">
                <div><a href="<?php echo $menu["url"]; ?>"><?php echo $menu["name"]; ?></a></div>
                <?php if(isset($menu["children"])): ?>
                <div class="submenu">
                    <?php foreach ($menu["children"] as $menu) : ?>
                    <div><a href="<?php echo $menu["url"]; ?>"><?php echo $menu["name"]; ?></a></div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>
            <?php endforeach; ?>
    </nav>
        
</header>
<script>
    
</script>