<?php
/**
 * Description of Index_controller
 *
 * @author pabhoz
 */

class Men_controller extends \Fox\FoxController{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->render($this,"index","DayaanShop | MEN");
    }
    
    public function footwear(){
        $this->view->render($this,"footwear","DayaanShop | MEN Footwear");
    }
    
}
