<?php
/**
 * Description of Index_controller
 *
 * @author pabhoz
 */

class Index_controller extends \Fox\FoxController{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->tendencias = Item::advancedWhere("1=1 LIMIT 10", []);
        $menus = Menus_bl::getMenus();
        $menus = Menus_bl::orderMenus($menus);
        $this->view->menus = $menus;
        $this->view->render($this,"index","View Title");
    }
    
}
